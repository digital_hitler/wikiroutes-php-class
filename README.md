# Class for using WikiRoutes.info API with PHP #

### What is this repository for? ###

Here are simple PHP class for workaround with [WikiRoutes](http://wikiroutes.info) API. It can authorize there, remember user session IDs, recover session if its expired and of course get any data from WikiRoutes in just classic PHP objects.
Simple debug console can be shown to developer.

### Links ###
* [WikiRoutes.info](http://wikiroutes.info) – perfect openly editable public transit routes guide.
* [API reference](http://wikiroutes.info/manual/#/api)

### Requirements ###
* PHP 5.5+ with session engine enabled
* cUrl extension
* Two hands

### Easy start ###
1. Clone repository or just upload **routes.php** where you need
2. Open **routes.php** and add your API key here:
```
#!php

class WikiRoutes {
    private $ApiKey = ''; // your API key

```
3. You can also change user agent and timeout if you need it.

### Usage ###
In your code create instance of WikiRoutes class and try to get some data with **GetData** method:

```
#!php

$Routes = new WikiRoutes();

$arrRoutes = $Routes->GetData ("getCatalog",
    array ("cityId" => 1)
);

var_dump($arrRoutes);
```

GetData ( **method** (string), *data* (array, not required) )
Where: 
* **method** – string, required: method name from API reference
* **data** – array, not required: parameters for API method (e.g. City ID or Lat/lon)

Returns *object* with JSON-decoded data or *false* if error happened.

### Debugging ###
Every step is recording. If something goes wrong, you can show debug console with **ShowConsole()** method:

```
#!php

$Routes->ShowConsole();
```

### License, thanks etc ###
I don't care about license or something like this. Use it as you want.

If you find some bug please report to issue tracker here.